#!/usr/bin/env python

####################################################################
#                                                                  #
# Purpose: Make R-scan in situ response plots for paper            #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

EtaBins               = [45,69] # 0 < eta < 0.1 and 2.4 < eta 2.5
JetColls              = ['2LC','6LC']
PATH2Inputs           = '/eos/atlas/atlascerngroupdisk/perf-jets/Rscan/R21/InsituInputs4Paper/'
PATH2AtlasStyleScript = 'FIXME/'

####################################################################
# DO NOT MODIFY (below this line)
####################################################################

import ROOT,os,sys,argparse

# Style
Ndivisions = 501
minY_pad2 = 0.851
maxY_pad2 = 1.199
yRanges_pad1 = {
  'Dijets' : {'2LC' : [0.8,1.3], '6LC' : [0.9,1.3]},
  'Zjets'  : {'2LC' : [0.7,1.2], '6LC' : [0.95,1.45]},
}
# minX and maxX
xRanges = {
  'Dijets' : { # Eta bin : [minX,maxX]
    45 : [45,1900],
    69 : [45,1900],
  },
 'Zjets' : { # Eta bin : [minX,maxX]
    45 : [20,300],
    69 : [20,300],
  },
}
# colors and markers
Colors  = {'Data':ROOT.kBlack,'Nominal':ROOT.kRed+1,'Sherpa':ROOT.kGreen+2}
Markers = {'Data':20,'Nominal':21,'Sherpa':22}

# Eta binning
EtaBining = [round(-4.5 + x*0.1,2) for x in range(91)]

# Create output folders
for coll in JetColls:
  os.system('mkdir -p Plots/{}'.format(coll))

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--topology', action='store',      dest="topology")
parser.add_argument('--atlas',    action='store',      dest="atlas")
parser.add_argument('--format',   action='store',      dest="outputFormat", default='PDF')
parser.add_argument('--debug',    action='store_true', dest="debug", default=False)
args = parser.parse_args()
# Protections
if args.topology is None:
  print('ERROR: topology (Dijets or Zjets) not provided, exiting')
  sys.exit(0)
if args.atlas is None:
  print('ERROR: ATLAS legend not selected, exiting')
  sys.exit(0)
Debug       = args.debug
Topology    = args.topology
ATLASlegend = args.atlas
Format      = {'PDF': 'pdf', 'PNG': 'png', 'JPG': 'jpg', 'EPS' : 'eps', 'C': 'C'}[args.outputFormat]

# AtlasStyle
ROOT.gROOT.LoadMacro("{}AtlasStyle.C".format(PATH2AtlasStyleScript))
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Extend path to inputs
PATH2Inputs += '{}/'.format(Topology)

# Loop over jet collections
for coll in JetColls:
  # Loop over eta bins
  for etabin in EtaBins:
    if Debug: print("###########################################################")
    if Debug: print("DEBUG: Producing plot for {} and eta bin {}".format(coll,etabin))

    ############################
    # Get Histograms
    ############################

    HistName = 'Response_vs_pTRef_Eta_{}'.format(etabin)

    # Loop over samples
    Hists = dict()
    for sample in ['Data','Nominal','Sherpa']:
      # Get response distribution
      if sample == 'Data':
        InputFileName = '{}Data/2015+2016data/{}/Outputs_vs_pT{}.root'.format(PATH2Inputs,coll,'_ZJetStudy' if Topology == 'Zjets' else '')
      elif sample == 'Nominal':
        InputFileName = '{}MC/MC16a/{}/MC16a_{}_Outputs_vs_pT.root'.format(PATH2Inputs,coll,'Pythia' if Topology == 'Dijets' else 'ZJetStudy')
      else:
        InputFileName = '{}MC/MC16a/{}/MC16a_{}_Outputs_vs_pT_MCSherpa.root'.format(PATH2Inputs,coll,'Pythia' if Topology == 'Dijets' else 'ZJetStudy')
      InputFile = ROOT.TFile.Open(InputFileName)
      if not InputFile:
        print('ERROR: {} not found, exiting'.format(InputFileName))
        sys.exit(0)
      Hist = InputFile.Get(HistName)
      if not Hist:
        print('ERROR: {} not found in {}, exiting'.format(HistName,InputFileName))
        sys.exit(0)
      Hist.SetDirectory(0)
      Hist.SetLineColor(Colors[sample])
      Hist.SetMarkerColor(Colors[sample])
      Hist.SetMarkerStyle(Markers[sample])
      Hists[sample] = Hist
      InputFile.Close()

    ########################
    # Make Plot
    ########################

    # TCanvas
    if Debug: print("DEBUG: Create TCanvas")
    Canvas  = ROOT.TCanvas()
    outName = "Plots/{}/{}_{}.{}".format(coll,Topology,etabin,Format)

    # TPad for upper panel
    if Debug: print("DEBUG: Create TPad for upper panel")
    pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
    pad1.SetTopMargin(0.08)
    pad1.SetBottomMargin(0.03)
    pad1.Draw()
    pad1.cd()

    pad1.SetLogx()

    # Legends
    Legends = ROOT.TLegend(0.75,0.6,0.92,0.9)
    Legends.SetTextFont(42)

    # Draw distributions
    counter = 0
    for sample in Hists:
      if counter == 0:
        Hists[sample].Draw("E P")
      else:
        Hists[sample].Draw("E P same")
      Hists[sample].GetXaxis().SetLabelSize(0.)
      Hists[sample].GetXaxis().SetTitleSize(0.)
      Hists[sample].GetYaxis().SetTitleSize(20)
      Hists[sample].GetYaxis().SetTitleFont(43)
      Hists[sample].GetYaxis().SetLabelFont(43)
      Hists[sample].GetYaxis().SetLabelSize(19)
      Hists[sample].GetYaxis().SetTitleOffset(1.3)
      Hists[sample].GetYaxis().SetTitle("#LT #it{p}_{T}^{rscan}/#it{p}_{T}^{ref} #GT")
      if sample == 'Data':
        legend = 'Data'
      elif sample == 'Nominal':
        legend = "Pythia" if Topology == 'Dijets' else 'Powheg+Pythia'
      else:
        legend = 'Sherpa'
      Legends.AddEntry(Hists[sample],legend,"p")
      Hists[sample].GetYaxis().SetRangeUser(yRanges_pad1[Topology][coll][0],yRanges_pad1[Topology][coll][1])
      Hists[sample].GetXaxis().SetNdivisions(Ndivisions)
      Hists[sample].GetXaxis().SetRangeUser(xRanges[Topology][etabin][0],xRanges[Topology][etabin][1])
      counter += 1

    if Debug: print("DEBUG: Draw legends")
    Legends.Draw("same")

    ROOT.gPad.RedrawAxis()

    # Show ATLAS legend
    if Debug: print("DEBUG: Show ATLAS legend")
    if ATLASlegend != 'NONE':
      if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
      elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
      elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
      else:
        print("ERROR: ATLASlegend not recognized, exiting")
        sys.exit(0)
      ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
      ATLASBlock.SetNDC()
      ATLASBlock.Draw("same")

    # Show CME, luminosity and topology
    if Debug: print("DEBUG: Show CME, luminosity and topology")
    Topo = 'Multijets' if Topology == 'Dijets' else '#it{Z}(#rightarrow#mu#mu) + jets'
    CME = "#scale[1.5]{13 TeV, 36.2 fb^{-1}, "+Topo+"}"
    topY = 0.7 if ATLASlegend != 'NONE' else 0.8
    CMEblock = ROOT.TLatex(0.2,topY,CME)
    CMEblock.SetNDC()
    CMEblock.Draw("same")

    # Show jet algorithm
    if Debug: print("DEBUG: Show jet algorithm")
    jetAlg = "#scale[1.5]{anti-#it{k}_{t} #it{R} = "
    jetAlg += "0.2" if coll == "2LC" else "0.6"
    jetAlg += ", LCW}"
    topY = 0.6 if ATLASlegend != 'NONE' else 0.7
    JetBlock = ROOT.TLatex(0.2,topY,jetAlg)
    JetBlock.SetNDC()
    JetBlock.Draw("same")

    # Show eta bin
    if Debug: print("DEBUG: Show eta bin")
    Bin = "#scale[1.5]{"+str(EtaBining[etabin])+" < #eta < "+str(EtaBining[etabin+1])+"}"
    topY = 0.5 if ATLASlegend != 'NONE' else 0.6
    TextBlock = ROOT.TLatex(0.2,topY,Bin)
    TextBlock.SetNDC()
    TextBlock.Draw("same")
  
    # TPad for bottom plot
    if Debug: print("DEBUG: Create TPad for bottom panel")
    Canvas.cd()
    pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
    pad2.SetTopMargin(0.03)
    pad2.SetBottomMargin(0.32)
    pad2.Draw()
    pad2.cd()
    pad2.SetLogx()

    # Create ratio histograms
    if Debug: print("DEBUG: Create MC/data histograms")
    ratioHists = dict()
    for sample in ['Nominal','Sherpa']:
      ratioHist = Hists[sample].Clone("ratioHist_{}".format(sample))
      ratioHist.SetLineColor(Colors[sample])
      ratioHist.SetMarkerColor(Colors[sample])
      ratioHist.SetMarkerStyle(Markers[sample])
      ratioHist.SetMinimum(minY_pad2)
      ratioHist.SetMaximum(maxY_pad2)
      ratioHist.GetXaxis().SetRangeUser(xRanges[Topology][etabin][0],xRanges[Topology][etabin][1])
      ratioHists[sample] = ratioHist

    # Draw data/MC ratios
    if Debug: print("DEBUG: Draw MC/data ratios")
    counter = 0
    for sample in ratioHists:
      if counter == 0:
        ratioHists[sample].Draw("e0")
      else:
        ratioHists[sample].Draw("e0 same")
      counter +=1

    # Draw line at data/MC==1
    if Debug: print("DEBUG: Draw line at MC/data==1")
    Line = ROOT.TLine(xRanges[Topology][etabin][0],1,xRanges[Topology][etabin][1],1)
    Line.SetLineStyle(7)
    Line.Draw("same")

    # Set x-axis title
    if Debug: print("DEBUG: Set X-axis title")
    for sample in ratioHists:
      ratioHists[sample].GetXaxis().SetTitleSize(20)
      ratioHists[sample].GetXaxis().SetTitleFont(43)
      ratioHists[sample].GetXaxis().SetLabelFont(43)
      ratioHists[sample].GetXaxis().SetLabelSize(19)
      ratioHists[sample].GetXaxis().SetTitleOffset(3.5)
      ratioHists[sample].GetXaxis().SetTitle('#it{p}_{T}^{ref} [GeV]')
      ratioHists[sample].GetYaxis().SetTitleSize(20)
      ratioHists[sample].GetYaxis().SetTitleFont(43)
      ratioHists[sample].GetYaxis().SetLabelFont(43)
      ratioHists[sample].GetYaxis().SetLabelSize(19)
      ratioHists[sample].GetYaxis().SetTitleOffset(1.3)
      ratioHists[sample].GetYaxis().SetTitle("MC / Data")
      ratioHists[sample].GetXaxis().SetNdivisions(501)

    # Save PDF
    if Debug: print("DEBUG: Save/print PDF")
    Canvas.Print(outName)
    for sample,hist in Hists.items():
      del hist
    for sample,hist in ratioHists.items():
      del hist

print('>>> DONE <<<')
